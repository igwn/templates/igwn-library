/**
 **********************************************
 *
 * \file libIGWN.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libIGWN.Config.h"

#pragma link off all globals;
#pragma link off all functions;

// #pragma link C++ namespace IGWN+;

#endif