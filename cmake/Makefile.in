.PHONY: env mamba mamba-check tests build install docs motd

export PROJECT_NAME = @PROJECT_NAME@
export PROJECT_TITLE = @PROJECT_TITLE@
export PROJECT_DESCRIPTION = @PROJECT_DESCRIPTION@
export PROJECT_VERSION = @PROJECT_VERSION@
export PROJECT_HOMEPAGE_URL = @PROJECT_HOMEPAGE_URL@

export ENVNAME ?= @PROJECT_NAME_LOWER@
export ENVFILE ?= environment.yml
export ENVACTIVATE = . $$(conda info --base)/etc/profile.d/conda.sh ; conda activate ; conda activate $(ENVNAME)

export MOTD_FILE ?= share/motd
usage: motd
	@echo "[Usage] make [all|...|env|build|tests|image|container|install|docs|clean]"

motd:
ifneq ($(MOTD),0)
	@if [[ -s "$(MOTD_FILE)" && ! -z "$$(which envsubst)" ]]; then echo "$$(env | envsubst < $(MOTD_FILE))"; fi
endif

all: motd env build install

# Create a Conda environment from environment.yml
env: motd
	@$(MAKE) .virtualenv

.virtualenv: $(ENVFILE)
	@$(MAKE) mamba-check 
	@if mamba env list | grep -q $(ENVNAME); then \
		echo "Updating existing environment \`$(ENVNAME)\`"; \
		mamba env update -n $(ENVNAME) -f $(ENVFILE); \
	else \
		echo "Creating new environment \`$(ENVNAME)\`"; \
		mamba env create -n $(ENVNAME) -f $(ENVFILE); \
	fi

	@touch .virtualenv

# Install Mamba using Conda
mamba: motd
	@echo "Installing Mamba..."
	@. $$HOME/.bashrc && conda install mamba -n base -c conda-forge --yes
	@echo "Mamba installation complete."

mamba-check:
	@command -v mamba >/dev/null 2>&1 || { echo "Error: mamba is not installed. Please install mamba to use this command."; exit 1; }

# Run tests
tests: motd
	@make -sC build tests

# Build the project documentation
pages: motd
	@MOTD=0 make -sC build pages
	@open public/latest/index.html
docs: motd
	@MOTD=0 make -sC build doxygen

build: motd
	@MOTD=0 make -sC build

install: motd
	@MOTD=0 make -sC build install

# Docker commands
image:
	@docker build -t $(ENVNAME) .
	@docker run -it --rm $(ENVNAME) /bin/bash

container:
	@CONTAINER_NAME=$$(basename $(PWD)-client) \
	 docker compose up client -d
	@CONTAINER_ID=$$(docker ps -aqf name=^$$(basename ${PWD})-client$$) && \
	 if [ -n "$$CONTAINER_ID" ]; then docker exec -it $$CONTAINER_ID /bin/bash; else echo "No container found"; fi
	@CONTAINER_NAME=$$(basename $(PWD)-client) \
     docker compose down client

# Clean up build artifacts
clean: motd
	@echo "Cleaning temporary files"
	@$(RM) -rf var/cache/* var/log/*
	@$(MAKE) -sC build clean 2> /dev/null || true

distclean: clean
	@echo "Removing environment \`$(ENVNAME)\`"
	@mamba env remove -n $(ENVNAME)
	@$(RM) -rf .virtualenv var/install var/build  Makefile public build
