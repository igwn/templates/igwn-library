# Include custom standard package
include(FindPackageStandard)

# Load using standard package finder
set(_KFR_CAPI kfr_capi)
set(_KFR_DSP kfr_dsp:kfr_dsp_${KFR_ARCH},kfr_dsp_neon64,libkfr_dsp_avx512,libkfr_dsp_avx2,libkfr_dsp_avx,libkfr_dsp_sse41,libkfr_dsp_sse2,kfr_dsp)
set(_KFR_DFT kfr_dft:kfr_dft_${KFR_ARCH},kfr_dft_neon64,libkfr_dft_avx512,libkfr_dft_avx2,libkfr_dft_avx,libkfr_dft_sse41,libkfr_dft_sse2,kfr_dft)

find_package_standard(
  NAMES kfr_io
  OPTIONALS ${_KFR_CAPI} ${_KFR_DSP} ${_KFR_DFT}
  HEADERS "kfr/kfr.h"
  PATHS ${KFR} $ENV{KFR}
)
