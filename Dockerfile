FROM rootproject/root:latest
#FROM igwn/software:buster

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV CXX="/usr/bin/clang++"
ENV CC="/usr/bin/clang"

COPY Dockerfile.packages packages
RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages | grep -v '#') && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

# Additional conda environment
RUN wget -O Miniforge3.sh "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
RUN sh Miniforge3.sh -b -p "/opt/conda" && rm Miniforge3.sh

ENV PATH /opt/conda/bin:$PATH

COPY environment.yml .
RUN mamba env create --file environment.yml && \
    mamba clean --all --yes

RUN conda init
RUN ENVNAME=$(cat environment.yml | grep 'name:' | cut -d':' -f2 | sed 's/[ ]*//') && \
    echo "conda activate $ENVNAME" >> ~/.bashrc

WORKDIR /opt

# Install ROOT+ library
RUN git clone --recursive https://git.ligo.org/kagra/libraries-addons/root/root-plus.git /opt/root-plus
RUN /opt/root-plus/cmake/download_libtorch 2.3.1
ENV PATH="/opt/libtorch/bin:${PATH}"
ENV LD_LIBRARY_PATH="/opt/libtorch/lib:${LD_LIBRARY_PATH}"

RUN cd /opt/root-plus && mkdir build && cd build && cmake ..
RUN cd /opt/root-plus/build && make -j$(nproc) && make -j$(nproc) install
RUN rm -rf /opt/root-plus

# Download and install KFR library
RUN git clone https://gitlab.cern.ch/igwn/software/kfr.git /opt/kfr
RUN mkdir -p /opt/kfr/build && cd /opt/kfr/build && cmake .. -DKFR_ENABLE_CAPI_BUILD=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON
RUN cd /opt/kfr/build && make -j$(nproc) && make -j$(nproc) install
RUN rm -rf /opt/kfr

# Download and install RD KAFKA (optional)
RUN wget https://github.com/confluentinc/librdkafka/archive/refs/tags/v2.3.0.tar.gz
RUN tar -xzf v2.3.0.tar.gz
RUN cd librdkafka-2.3.0 && mkdir build 
RUN cd librdkafka-2.3.0/build && cmake ..
RUN cd librdkafka-2.3.0/build && make -j$(nproc)
RUN cd librdkafka-2.3.0/build && make -j$(nproc) install
RUN rm -rf librdkafka-2.3.0 v2.3.0.tar.gz

# Download and install FrameL library
RUN git clone https://git.ligo.org/virgo/virgoapp/Fr.git /opt/framel-src
RUN cd /opt/framel-src && mkdir build
RUN cd /opt/framel-src/build && cmake ..
RUN cd /opt/framel-src/build && make -j$(nproc)
RUN cd /opt/framel-src/build && make -j$(nproc) install
RUN rm -rf framel-src

# Download and install metaio
RUN git clone https://git.ligo.org/lscsoft/metaio.git
RUN cd metaio && git checkout release-8.5.0-v1 
RUN cd metaio && ./00boot 
RUN cd metaio && ./configure
RUN cd metaio && make -j$(nproc)
RUN cd metaio && make -j$(nproc) install
RUN rm -rf metaio

# Download and install lalsuite (including some custom fixes to be corrected)
RUN pip install six numpy
ENV PYTHONPATH=${PYTHONPATH}/usr/local/lib/python3.10/dist-packages

RUN git clone --recursive https://git.ligo.org/lscsoft/lalsuite.git /opt/lalsuite-src
RUN cd /opt/lalsuite-src &&  ./00boot
RUN cd /opt/lalsuite-src &&  ./configure --prefix=/usr/local
RUN cd /opt/lalsuite-src &&  make -j$(nproc)
RUN cd /opt/lalsuite-src &&  make -j$(nproc) install
RUN rm -rf lalsuite-src

# Download and install HDF5 library (optional)
RUN wget https://github.com/HDFGroup/hdf5/archive/refs/tags/hdf5-1_14_3.tar.gz
RUN tar -xzf hdf5-1_14_3.tar.gz
RUN mkdir -p hdf5-hdf5-1_14_3/build
RUN cd hdf5-hdf5-1_14_3/build && cmake .. -DHDF5_BUILD_CPP_LIB=ON -DCMAKE_INSTALL_PREFIX=/usr/local
RUN cd hdf5-hdf5-1_14_3/build && make -j$(nproc)
RUN cd hdf5-hdf5-1_14_3/build && make -j$(nproc) install
RUN rm -rf hdf5-hdf5-1_14_3 hdf5-1_14_3.tar.gz

# Prepare igwn environment for python users
COPY environment.yml .
ENV ENVNAME=myigwn
RUN mamba env create -n ${ENVNAME} --file environment.yml && \
    mamba clean --all --yes

RUN conda init
RUN echo "conda activate ${ENVNAME}" >> ~/.bashrc

WORKDIR /root
CMD ["/bin/bash"]
